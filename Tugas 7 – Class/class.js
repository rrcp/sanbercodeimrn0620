//no 1
class Animal {
    constructor(name,legs=4, cold_blooded='false'){
        this.nama = name;
        this.kaki = legs;
        this.darah_dingin = cold_blooded;
    }
    name(){
        return this.nama;
    }
    legs(){
        return this.kaki;
    }
    cold_blooded() {
        return false;
    }
}

var sheep = new Animal("shaun");
console.log(sheep.name())
console.log(sheep.legs())
console.log(sheep.cold_blooded())

class Ape extends Animal {
    yell() {
        console.log('Auooo');
    }
}

class Frog extends Animal {
    jump() {
        console.log('hop hop');
    }
}

var sungokong = new Ape("kera sakti")
sungokong.yell()

var kodok = new Frog("buduk")
kodok.jump()

//no2
class Clock {
    constructor({template}) {
        this.template = template;
    }
    render() {
        var date = new Date();

        let hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        let mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        let secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
        let output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);

        console.log(output);
    }
    stop() {
        clearInterval(this.timer);
    }
    start() {
        this.render();
        this.timer = setInterval(() => this.render(), 1000);
    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start(); 

