console.log("LOOPING PERTAMA");
var flag = 2;
while(flag <= 20) { // Loop akan terus berjalan selama nilai flag masih dibawah 10
  console.log(flag + ' - I love coding'); // Menampilkan nilai flag pada iterasi tertentu
  flag = flag+2; // Mengubah nilai flag dengan menambahkan 1
}

console.log("\nLOOPING KEDUA");
var flag = 20;
while(flag >= 1) { // Loop akan terus berjalan selama nilai flag masih dibawah 10
  console.log(flag + ' - I will become a mobile developer'); // Menampilkan nilai flag pada iterasi tertentu
  flag = flag-2; // Mengubah nilai flag dengan menambahkan 1
}

console.log("\nNo. 2 Looping menggunakan for");
for(var angka = 1; angka <= 20; angka++) {
  if(angka % 2 == 1){
    if(angka % 3 == 0){
      console.log(angka + ' - I Love Coding');
    }else{
      console.log(angka + ' - Santai');
    }
  }else{
    console.log(angka + ' - Berkualitas');
  }
} 

console.log("\nNo. 3 Membuat Persegi Panjang #");
var tampung = '';
for(var baris = 1; baris <= 4; baris++){
  for(var kolom = 1; kolom <= 8; kolom++){
    tampung += '#';
  }
  tampung += '\n';
}
console.log(tampung);

console.log("No. 4 Membuat Tangga");
var tampung = '';
for(var baris = 1; baris <= 7; baris++){
  for(var kolom = 1; kolom <= baris; kolom++){
    tampung += '#';
  }
  tampung += '\n';
}
console.log(tampung);

console.log("No. 5 Membuat Papan Catur");
var tampung = '';
for(var baris = 1; baris <= 8; baris++){
  for(var kolom = 1; kolom <= 8; kolom++){
    if(baris % 2 == 0){
      if(kolom % 2 == 0){
        tampung += ' ';
      }else{
        tampung += '#';
      }
    }else{
      if(kolom % 2 == 0){
        tampung += '#';
      }else{
        tampung += ' ';
      }
    }
  }
  tampung += '\n';
}
console.log(tampung);

