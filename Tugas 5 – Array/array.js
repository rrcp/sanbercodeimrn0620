//no1
function range(startNum, finishNum) {
  var numbers = []
  if(startNum < finishNum){ //jika angka awal lebih kecil dari angka akhir
  	for(x = startNum; x <= finishNum; x++){ //x++ -> nambah jadi bro
  		numbers.push(x);
  	}
  }else if(startNum > finishNum){
	for(x = startNum; x >= finishNum; x--){ //x-- -> jadi kurang
  		numbers.push(x);
  	}
  }else{
  	numbers = -1
  }

  return numbers
}
console.log(range())

//no2
function rangeWithStep(startNum, finishNum, step) {
  var numbers = []
  if(startNum < finishNum){
  	for(x = startNum; x <= finishNum; x+=step){
  		numbers.push(x);
  	}
  }else if(startNum > finishNum){
	for(x = startNum; x >= finishNum; x-=step){
  		numbers.push(x);
  	}
  }else{
  	numbers = -1
  }

  return numbers
}
console.log(rangeWithStep(29,2,4))
	
//no3
function sum(startNum, finishNum, step=1){
	if(!startNum){
		return 0
	}else if(!finishNum){
		return 1
	}else{
		numbers = rangeWithStep(startNum, finishNum, step)
		return numbers.reduce((a,b) => a + b,0)
	}

}
console.log(sum())

//no4--
var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ] 

function dataHandling(input){
	for(i=0;i<4;i++){
		console.log("Nomor ID: "+input[i][0]);
		console.log("Nama Lengkap: "+input[i][1]);
		console.log("TTL: "+input[i][2]+" "+input[i][3]);
		console.log("Hobi: "+input[i][4]);
	}
}
dataHandling(input)

//no5
function balikKata(kata){	
	var reverse = '';

	for (i = kata.length - 1; i >= 0; i--) {
  	reverse = reverse + kata[i];
 }

 return reverse;
}
console.log(balikKata("I am Sanbers"))

//no6
var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]  
function dataHandling2(input){
	input.splice(1, 1, "Roman Alamsyah Elsharawy")
	input.splice(2, 1, "Provinsi Bandar Lampung")
	input.splice(4, 0, "Pria")
	input.splice(5, 0, "SMA Internasional Metro")
	input.splice(6, 1)
	console.log(input)

	var ttl = input[3]
	var split = ttl.split("/")
	var bulan = split[1]
	switch(bulan) {
	  case "01":   { bulan = "Januari"; break; }
	  case "02":   { bulan = "Februari"; break; }
	  case "03":   { bulan = "Maret"; break; }
	  case "04":   { bulan = "April"; break; }
	  case "05":   { bulan = "Mei"; break; }
	  case "06":   { bulan = "Juni"; break; }
	  case "07":   { bulan = "Juli"; break; }
	  case "08":   { bulan = "Agustus"; break; }
	  case "09":   { bulan = "September"; break; }
	  case "10":   { bulan = "Oktober"; break; }
	  case "11":   { bulan = "November"; break; }
	  case "12":   { bulan = "Desember"; break; }
	}
	console.log(bulan)

	split.sort(function(a, b) { return b - a })
	console.log(split)

	split.sort(function(a, b) { return a - b })
	slug = split.join("-")
	console.log(slug)

	var irisan = input[1].slice(0,15)
	console.log(irisan) 

}

dataHandling2(input)






