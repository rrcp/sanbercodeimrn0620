//no 1
function arrayToObject(arr){
	var obj = {}
	var no = 1
	var now = new Date()
	var thisYear = now.getFullYear()
	for(var x=0; x <= arr.length-1; x++){
		if(!arr[x][3] || (arr[x][3] > thisYear)){
			age = "Invalid birth year"
		}else{
			age = thisYear - arr[x][3]
		}
		
		obj.firstName = arr[x][0]
		obj.lastName = arr[x][1]
		obj.gender = arr[x][2]
		obj.age = age
		console.log(no+". "+arr[x][0]+" "+arr[x][1]+": ",obj);
		no++;
	}
}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 

//no 2
function shoppingTime(memberId, money) {
    var listItems = {   
        'Sepatu Stacattu': 1500000,
        'Baju Zoro': 500000,
        'Baju H&N' : 250000,
        'Sweater Uniklooh': 175000,
        'Casing Handphone' : 50000
    };
    var listPurchased = []
    var total = 0
    var currentMoney = money
    var lowerLimit = 50000

    if (!memberId || memberId === '') { 
        return 'Mohon maaf, toko X hanya berlaku untuk member saja'
    } else if (money < lowerLimit) {  
        return 'Mohon maaf, uang tidak cukup'
    }

    Object.entries(listItems).map(([key, val]) => {
        if (currentMoney >= val) {
            listPurchased.push(key)
            currentMoney -= val
            total += val
        }
    })
    return {
        memberId: memberId,
        money: money,
        listPurchased: listPurchased,
        changeMoney: money - total
    }

}

console.log(shoppingTime('', 2475000));
console.log(shoppingTime('234JdhweRxa53', 15000));
console.log(shoppingTime());

//no 3
function naikAngkot(arrPenumpang) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F']; 
    var pricePerRoute = 2000
    var result = []

    arrPenumpang.forEach(el => {
        var name = el[0]
        var start = el[1]
        var finish = el[2]
        var distance = Math.abs(rute.indexOf(start) - rute.indexOf(finish))
        var price = distance * pricePerRoute
        result.push({ 
            'penumpang': name,
            'naikDari': start,
            'tujuan': finish,
            'bayar': price
        });
    });

    return result

}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([]));



